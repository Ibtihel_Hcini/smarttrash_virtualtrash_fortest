package com.example.ibtihel.virtualTrash;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Zone6Activity extends AppCompatActivity {





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone6);
        // DatabaseReference
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Poubelle 1
        final Switch t6_1=findViewById(R.id.t6_1);
        mDatabase.child("VirtualTrashs").child("t6_1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t6_1.setChecked(true);
                else
                    t6_1.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t6_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t6_1").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t6_1").setValue(0);
            }
        });



        //Poubelle 2
        final Switch t6_2=findViewById(R.id.t6_2);
        mDatabase.child("VirtualTrashs").child("t6_2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t6_2.setChecked(true);
                else
                    t6_2.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t6_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t6_2").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t6_2").setValue(0);
            }
        });



        //Poubelle 3
        final Switch t6_3=findViewById(R.id.t6_3);
        mDatabase.child("VirtualTrashs").child("t6_3").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t6_3.setChecked(true);
                else
                    t6_3.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t6_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t6_3").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t6_3").setValue(0);
            }
        });



        //Poubelle 4
        final Switch t6_4=findViewById(R.id.t6_4);
        mDatabase.child("VirtualTrashs").child("t6_4").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t6_4.setChecked(true);
                else
                    t6_4.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t6_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t6_4").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t6_4").setValue(0);
            }
        });



        //Poubelle 5
        final Switch t6_5=findViewById(R.id.t6_5);
        mDatabase.child("VirtualTrashs").child("t6_5").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t6_5.setChecked(true);
                else
                    t6_5.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t6_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t6_5").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t6_5").setValue(0);
            }
        });



        //Poubelle 6
        final Switch t6_6=findViewById(R.id.t6_6);
        mDatabase.child("VirtualTrashs").child("t6_6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t6_6.setChecked(true);
                else
                    t6_6.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t6_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t6_6").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t6_6").setValue(0);
            }
        });


    }




}


