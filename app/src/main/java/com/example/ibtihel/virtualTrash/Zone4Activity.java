package com.example.ibtihel.virtualTrash;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Zone4Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone4);
        // DatabaseReference
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Poubelle 1
        final Switch t4_1=findViewById(R.id.t4_1);
        mDatabase.child("VirtualTrashs").child("t4_1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_1.setChecked(true);
                else
                    t4_1.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_1").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_1").setValue(0);
            }
        });



        //Poubelle 2
        final Switch t4_2=findViewById(R.id.t4_2);
        mDatabase.child("VirtualTrashs").child("t4_2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_2.setChecked(true);
                else
                    t4_2.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_2").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_2").setValue(0);
            }
        });



        //Poubelle 3
        final Switch t4_3=findViewById(R.id.t4_3);
        mDatabase.child("VirtualTrashs").child("t4_3").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_3.setChecked(true);
                else
                    t4_3.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_3").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_3").setValue(0);
            }
        });



        //Poubelle 4
        final Switch t4_4=findViewById(R.id.t4_4);
        mDatabase.child("VirtualTrashs").child("t4_4").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_4.setChecked(true);
                else
                    t4_4.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_4").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_4").setValue(0);
            }
        });



        //Poubelle 5
        final Switch t4_5=findViewById(R.id.t4_5);
        mDatabase.child("VirtualTrashs").child("t4_5").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_5.setChecked(true);
                else
                    t4_5.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_5").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_5").setValue(0);
            }
        });



        //Poubelle 6
        final Switch t4_6=findViewById(R.id.t4_6);
        mDatabase.child("VirtualTrashs").child("t4_6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_6.setChecked(true);
                else
                    t4_6.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_6").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_6").setValue(0);
            }
        });


        //Poubelle 7
        final Switch t4_7=findViewById(R.id.t4_7);
        mDatabase.child("VirtualTrashs").child("t4_7").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_7.setChecked(true);
                else
                    t4_7.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_7").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_7").setValue(0);
            }
        });


        //Poubelle 8
        final Switch t4_8=findViewById(R.id.t4_8);
        mDatabase.child("VirtualTrashs").child("t4_8").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_8.setChecked(true);
                else
                    t4_8.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_8").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_8").setValue(0);
            }
        });

        //Poubelle 9
        final Switch t4_9=findViewById(R.id.t4_9);
        mDatabase.child("VirtualTrashs").child("t4_9").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_9.setChecked(true);
                else
                    t4_9.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_9").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_9").setValue(0);
            }
        });


        //Poubelle 10
        final Switch t4_10=findViewById(R.id.t4_10);
        mDatabase.child("VirtualTrashs").child("t4_10").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_10.setChecked(true);
                else
                    t4_10.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_10.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_10").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_10").setValue(0);
            }
        });


        //Poubelle 11
        final Switch t4_11=findViewById(R.id.t4_11);
        mDatabase.child("VirtualTrashs").child("t4_11").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_11.setChecked(true);
                else
                    t4_11.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_11.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_11").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_11").setValue(0);
            }
        });



        //Poubelle 12
        final Switch t4_12=findViewById(R.id.t4_12);
        mDatabase.child("VirtualTrashs").child("t4_12").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_12.setChecked(true);
                else
                    t4_12.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_12.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_12").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_12").setValue(0);
            }
        });


        //Poubelle 13
        final Switch t4_13=findViewById(R.id.t4_13);
        mDatabase.child("VirtualTrashs").child("t4_13").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_13.setChecked(true);
                else
                    t4_13.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_13.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_13").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_13").setValue(0);
            }
        });


        //Poubelle 14
        final Switch t4_14=findViewById(R.id.t4_14);
        mDatabase.child("VirtualTrashs").child("t4_14").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t4_14.setChecked(true);
                else
                    t4_14.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t4_14.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t4_14").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t4_14").setValue(0);
            }
        });


    }



}


