package com.example.ibtihel.virtualTrash;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Zone5Activity extends AppCompatActivity {





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone5);
        // DatabaseReference
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Poubelle 1
        final Switch t5_1=findViewById(R.id.t5_1);
        mDatabase.child("VirtualTrashs").child("t5_1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t5_1.setChecked(true);
                else
                    t5_1.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t5_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t5_1").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t5_1").setValue(0);
            }
        });



        //Poubelle 2
        final Switch t5_2=findViewById(R.id.t5_2);
        mDatabase.child("VirtualTrashs").child("t5_2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t5_2.setChecked(true);
                else
                    t5_2.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t5_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t5_2").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t5_2").setValue(0);
            }
        });



        //Poubelle 3
        final Switch t5_3=findViewById(R.id.t5_3);
        mDatabase.child("VirtualTrashs").child("t5_3").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t5_3.setChecked(true);
                else
                    t5_3.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t5_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t5_3").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t5_3").setValue(0);
            }
        });



        //Poubelle 4
        final Switch t5_4=findViewById(R.id.t5_4);
        mDatabase.child("VirtualTrashs").child("t5_4").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t5_4.setChecked(true);
                else
                    t5_4.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t5_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t5_4").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t5_4").setValue(0);
            }
        });



        //Poubelle 5
        final Switch t5_5=findViewById(R.id.t5_5);
        mDatabase.child("VirtualTrashs").child("t5_5").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t5_5.setChecked(true);
                else
                    t5_5.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t5_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t5_5").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t5_5").setValue(0);
            }
        });



        //Poubelle 6
        final Switch t5_6=findViewById(R.id.t5_6);
        mDatabase.child("VirtualTrashs").child("t5_6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t5_6.setChecked(true);
                else
                    t5_6.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t5_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t5_6").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t5_6").setValue(0);
            }
        });


    }




}


