package com.example.ibtihel.virtualTrash;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Zone3Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone3);
        // DatabaseReference
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Poubelle 1
        final Switch t3_1=findViewById(R.id.t3_1);
        mDatabase.child("VirtualTrashs").child("t3_1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_1.setChecked(true);
                else
                    t3_1.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_1").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_1").setValue(0);
            }
        });



        //Poubelle 2
        final Switch t3_2=findViewById(R.id.t3_2);
        mDatabase.child("VirtualTrashs").child("t3_2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_2.setChecked(true);
                else
                    t3_2.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_2").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_2").setValue(0);
            }
        });



        //Poubelle 3
        final Switch t3_3=findViewById(R.id.t3_3);
        mDatabase.child("VirtualTrashs").child("t3_3").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_3.setChecked(true);
                else
                    t3_3.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_3").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_3").setValue(0);
            }
        });



        //Poubelle 4
        final Switch t3_4=findViewById(R.id.t3_4);
        mDatabase.child("VirtualTrashs").child("t3_4").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_4.setChecked(true);
                else
                    t3_4.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_4").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_4").setValue(0);
            }
        });



        //Poubelle 5
        final Switch t3_5=findViewById(R.id.t3_5);
        mDatabase.child("VirtualTrashs").child("t3_5").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_5.setChecked(true);
                else
                    t3_5.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_5").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_5").setValue(0);
            }
        });



        //Poubelle 6
        final Switch t3_6=findViewById(R.id.t3_6);
        mDatabase.child("VirtualTrashs").child("t3_6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_6.setChecked(true);
                else
                    t3_6.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_6").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_6").setValue(0);
            }
        });


        //Poubelle 7
        final Switch t3_7=findViewById(R.id.t3_7);
        mDatabase.child("VirtualTrashs").child("t3_7").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_7.setChecked(true);
                else
                    t3_7.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_7").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_7").setValue(0);
            }
        });


        //Poubelle 8
        final Switch t3_8=findViewById(R.id.t3_8);
        mDatabase.child("VirtualTrashs").child("t3_8").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_8.setChecked(true);
                else
                    t3_8.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_8").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_8").setValue(0);
            }
        });

        //Poubelle 9
        final Switch t3_9=findViewById(R.id.t3_9);
        mDatabase.child("VirtualTrashs").child("t3_9").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_9.setChecked(true);
                else
                    t3_9.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_9").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_9").setValue(0);
            }
        });


        //Poubelle 10
        final Switch t3_10=findViewById(R.id.t3_10);
        mDatabase.child("VirtualTrashs").child("t3_10").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_10.setChecked(true);
                else
                    t3_10.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_10.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_10").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_10").setValue(0);
            }
        });


        //Poubelle 11
        final Switch t3_11=findViewById(R.id.t3_11);
        mDatabase.child("VirtualTrashs").child("t3_11").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_11.setChecked(true);
                else
                    t3_11.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_11.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_11").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_11").setValue(0);
            }
        });



        //Poubelle 12
        final Switch t3_12=findViewById(R.id.t3_12);
        mDatabase.child("VirtualTrashs").child("t3_12").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_12.setChecked(true);
                else
                    t3_12.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_12.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_12").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_12").setValue(0);
            }
        });


        //Poubelle 13
        final Switch t3_13=findViewById(R.id.t3_13);
        mDatabase.child("VirtualTrashs").child("t3_13").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_13.setChecked(true);
                else
                    t3_13.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_13.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_13").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_13").setValue(0);
            }
        });


        //Poubelle 14
        final Switch t3_14=findViewById(R.id.t3_14);
        mDatabase.child("VirtualTrashs").child("t3_14").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_14.setChecked(true);
                else
                    t3_14.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_14.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_14").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_14").setValue(0);
            }
        });

        //Poubelle 15
        final Switch t3_15=findViewById(R.id.t3_15);
        mDatabase.child("VirtualTrashs").child("t3_15").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t3_15.setChecked(true);
                else
                    t3_15.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t3_15.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t3_15").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t3_15").setValue(0);
            }
        });
        

    }





}


