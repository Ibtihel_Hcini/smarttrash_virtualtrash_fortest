package com.example.ibtihel.virtualTrash;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Zone1Activity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone1);
        // DatabaseReference
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Poubelle 1
        final Switch t1_1=findViewById(R.id.t1_1);
        mDatabase.child("VirtualTrashs").child("t1_1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    String value = dataSnapshot.getValue().toString();
                    if(value.equals("1"))
                        t1_1.setChecked(true);
                    else
                        t1_1.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t1_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                 mDatabase.child("VirtualTrashs").child("t1_1").setValue(1);
                else
                 mDatabase.child("VirtualTrashs").child("t1_1").setValue(0);
            }
        });



        //Poubelle 2
        final Switch t1_2=findViewById(R.id.t1_2);
        mDatabase.child("VirtualTrashs").child("t1_2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t1_2.setChecked(true);
                else
                    t1_2.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t1_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t1_2").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t1_2").setValue(0);
            }
        });



        //Poubelle 3
        final Switch t1_3=findViewById(R.id.t1_3);
        mDatabase.child("VirtualTrashs").child("t1_3").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t1_3.setChecked(true);
                else
                    t1_3.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t1_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t1_3").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t1_3").setValue(0);
            }
        });



        //Poubelle 4
        final Switch t1_4=findViewById(R.id.t1_4);
        mDatabase.child("VirtualTrashs").child("t1_4").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t1_4.setChecked(true);
                else
                    t1_4.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t1_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t1_4").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t1_4").setValue(0);
            }
        });



        //Poubelle 5
        final Switch t1_5=findViewById(R.id.t1_5);
        mDatabase.child("VirtualTrashs").child("t1_5").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t1_5.setChecked(true);
                else
                    t1_5.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t1_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t1_5").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t1_5").setValue(0);
            }
        });



        //Poubelle 6
        final Switch t1_6=findViewById(R.id.t1_6);
        mDatabase.child("VirtualTrashs").child("t1_6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t1_6.setChecked(true);
                else
                    t1_6.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t1_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t1_6").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t1_6").setValue(0);
            }
        });


    }




}


