package com.example.ibtihel.virtualTrash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private CardView zone1_card,zone2_card, zone3_card,  zone4_card, zone5_card, zone6_card,zone7_card,  infos_card;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // defining cards
        zone1_card = (CardView) findViewById(R.id.zone1);
        zone2_card = (CardView) findViewById(R.id.zone2);
        zone3_card = (CardView) findViewById(R.id.zone3);
        zone4_card = (CardView) findViewById(R.id.zone4);
        zone5_card = (CardView) findViewById(R.id.zone5);
        zone6_card = (CardView) findViewById(R.id.zone6);
        zone7_card = (CardView) findViewById(R.id.zone7);

        infos_card = (CardView) findViewById(R.id.info_card);

        // add action listner

        zone1_card.setOnClickListener(this);
        zone2_card.setOnClickListener(this);
        zone3_card.setOnClickListener(this);
        zone4_card.setOnClickListener(this);
        zone5_card.setOnClickListener(this);
        zone6_card.setOnClickListener(this);
        zone7_card.setOnClickListener(this);

        infos_card.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent i ;
        switch ( v.getId()) {
            case  R.id.zone1 :
                i= new Intent(this, Zone1Activity.class);
                startActivity(i);
                break;

            case  R.id.zone2 :
                i= new Intent(this, Zone2Activity.class);
                startActivity(i);
                break;

            case  R.id.zone3 :
                i= new Intent(this, Zone3Activity.class);
                startActivity(i);
                break;

            case  R.id.zone4 :
                i= new Intent(this, Zone4Activity.class);
                startActivity(i);
                break;

            case  R.id.zone5 :
                i= new Intent(this, Zone5Activity.class);
                startActivity(i);
                break;

            case  R.id.zone6 :
                i= new Intent(this, Zone6Activity.class);
                startActivity(i);
                break;

            case  R.id.zone7 :
                i= new Intent(this, Zone7Activity.class);
                startActivity(i);
                break;

            case  R.id.info_card :
                i= new Intent(this, InfoActivity.class);
                startActivity(i);
                break;


        }


    }
}
