package com.example.ibtihel.virtualTrash;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class Zone7Activity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone7);
        // DatabaseReference
        final DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Poubelle 1
        final Switch t7_1=findViewById(R.id.t7_1);
        mDatabase.child("VirtualTrashs").child("t7_1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t7_1.setChecked(true);
                else
                    t7_1.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t7_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t7_1").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t7_1").setValue(0);
            }
        });



        //Poubelle 2
        final Switch t7_2=findViewById(R.id.t7_2);
        mDatabase.child("VirtualTrashs").child("t7_2").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t7_2.setChecked(true);
                else
                    t7_2.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t7_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t7_2").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t7_2").setValue(0);
            }
        });



        //Poubelle 3
        final Switch t7_3=findViewById(R.id.t7_3);
        mDatabase.child("VirtualTrashs").child("t7_3").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t7_3.setChecked(true);
                else
                    t7_3.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t7_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t7_3").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t7_3").setValue(0);
            }
        });



        //Poubelle 4
        final Switch t7_4=findViewById(R.id.t7_4);
        mDatabase.child("VirtualTrashs").child("t7_4").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t7_4.setChecked(true);
                else
                    t7_4.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t7_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t7_4").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t7_4").setValue(0);
            }
        });



        //Poubelle 5
        final Switch t7_5=findViewById(R.id.t7_5);
        mDatabase.child("VirtualTrashs").child("t7_5").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t7_5.setChecked(true);
                else
                    t7_5.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t7_5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t7_5").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t7_5").setValue(0);
            }
        });



        //Poubelle 6
        final Switch t7_6=findViewById(R.id.t7_6);
        mDatabase.child("VirtualTrashs").child("t7_6").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue().toString();
                if(value.equals("1"))
                    t7_6.setChecked(true);
                else
                    t7_6.setChecked(false);
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                throw firebaseError.toException();

            }
        });
        t7_6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                    mDatabase.child("VirtualTrashs").child("t7_6").setValue(1);
                else
                    mDatabase.child("VirtualTrashs").child("t7_6").setValue(0);
            }
        });


    }




}


